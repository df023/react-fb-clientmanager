import { createStore, compose, combineReducers } from "redux";
import firebase from "firebase";
import "firebase/firestore";
import { reactReduxFirebase, firebaseReducer } from "react-redux-firebase";
import { reduxFirestore, firestoreReducer } from "redux-firestore";
// reducers
import notifyReducer from "./reducers/notifyReducer";
import settingsReducer from "./reducers/settingsReducer";

const firebaseConfig = {
  apiKey: "AIzaSyDpuysdxnrfu2lhS_uAA6QayHjmBMcqvTM",
  authDomain: "testproject-91ed8.firebaseapp.com",
  databaseURL: "https://testproject-91ed8.firebaseio.com",
  projectId: "testproject-91ed8",
  storageBucket: "testproject-91ed8.appspot.com",
  messagingSenderId: "816135321103"
};

const rrfConfig = {
  userProfile: "users",
  useFirestoreForProfile: true
};

// init firebase
firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();
firestore.settings({ timestampsInSnapshots: true });

// Add reactReduxFirebase enchancer when making store creator
const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig),
  reduxFirestore(firebase)
)(createStore);

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  notify: notifyReducer,
  settings: settingsReducer
});

if (!localStorage.getItem("settings")) {
  localStorage.setItem(
    "settings",
    JSON.stringify({
      disableBalanceOnAdd: true,
      disableBalanceOnEdit: false,
      allowRegistration: false
    })
  );
}

const initialState = { settings: JSON.parse(localStorage.getItem("settings")) };

// Create store
const store = createStoreWithFirebase(
  rootReducer,
  initialState,
  reactReduxFirebase(firebase)
);

export default store;
