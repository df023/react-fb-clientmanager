import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  setDisableBalanceOnAdd,
  setDisableBalanceOnEdit,
  setAllowRegistration
} from "../../actions/settingsActions";

class Settings extends Component {
  disableBalanceOnAddChange = () => {
    this.props.setDisableBalanceOnAdd();
  };
  disableBalanceOnEditChange = () => {
    this.props.setDisableBalanceOnEdit();
  };
  allowRegistrationChange = () => {
    this.props.setAllowRegistration();
  };

  render() {
    const {
      disableBalanceOnAdd,
      disableBalanceOnEdit,
      allowRegistration
    } = this.props.settings;
    return (
      <div>
        <div className="row">
          <div className="col-md-6">
            <Link to="/" className="btn btn-link">
              <i className="fas fa-arrow-circle-left" /> Back to Dashboard
            </Link>
          </div>
        </div>

        <div className="card">
          <div className="card-header">Edit Settings</div>
          <div className="card-body">
            <form>
              <div className="custom-control custom-checkbox">
                <input
                  type="checkbox"
                  id="allowReg"
                  name="allowRegistration"
                  className="custom-control-input"
                  checked={!!allowRegistration}
                  onChange={this.allowRegistrationChange}
                />
                <label
                  className="custom-control-label"
                  htmlFor="allowReg"
                  style={{ cursor: "pointer" }}
                >
                  Allow Registration
                </label>
              </div>
              <div className="custom-control custom-checkbox">
                <input
                  type="checkbox"
                  name="disableBalanceOnAdd"
                  id="disableOnAdd"
                  className="custom-control-input"
                  checked={!!disableBalanceOnAdd}
                  onChange={this.disableBalanceOnAddChange}
                />
                <label
                  className="custom-control-label"
                  htmlFor="disableOnAdd"
                  style={{ cursor: "pointer" }}
                >
                  Disable Balance On Add
                </label>
              </div>
              <div className="custom-control custom-checkbox">
                <input
                  type="checkbox"
                  id="disableOnEdit"
                  name="disableBalanceOnEdit"
                  className="custom-control-input"
                  checked={!!disableBalanceOnEdit}
                  onChange={this.disableBalanceOnEditChange}
                />
                <label
                  className="custom-control-label"
                  htmlFor="disableOnEdit"
                  style={{ cursor: "pointer" }}
                >
                  Disable Balance On Edit
                </label>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Settings.propTypes = {
  settings: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  setDisableBalanceOnAdd: PropTypes.func.isRequired,
  setDisableBalanceOnEdit: PropTypes.func.isRequired,
  setAllowRegistration: PropTypes.func.isRequired
};

export default connect(
  state => ({
    auth: state.firebase.auth,
    settings: state.settings
  }),
  { setDisableBalanceOnAdd, setDisableBalanceOnEdit, setAllowRegistration }
)(Settings);
